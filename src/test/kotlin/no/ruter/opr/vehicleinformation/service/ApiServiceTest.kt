package no.ruter.opr.vehicleinformation.service

import no.ruter.opr.vehicleinformation.serializer.VehicleSerializer
import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
internal class ApiServiceTest(
    @Autowired private val apiService: ApiService
) {
    @Test
    fun getOperators() {
        val operators = apiService.getOperators()
        assertNotNull(operators)
        assertTrue(operators.size > 0)
    }

    @Test
    fun getVehicles() {
        VehicleSerializer.refreshMetadata()
        val vehicles = apiService.getVehicles()
        assertNotNull(vehicles)
        assertTrue(vehicles.size > 0)
    }

    @Test
    fun getVehicleTypes() {
        val vehicleTypes = apiService.getVehicleTypes()
        assertNotNull(vehicleTypes)
        assertTrue(vehicleTypes.size > 0)
    }

    @Test
    fun getEngineTypes() {
        val engineTypes = apiService.getEngineTypes()
        assertNotNull(engineTypes)
        assertTrue(engineTypes.size > 0)
    }
}