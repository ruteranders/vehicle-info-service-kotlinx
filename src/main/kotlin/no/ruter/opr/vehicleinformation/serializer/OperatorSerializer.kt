package no.ruter.opr.vehicleinformation.serializer

import no.ruter.opr.vehicleinformation.domain.Operator
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object OperatorSerializer : KSerializer<Operator> {
    override val descriptor: SerialDescriptor = FridaContractor.serializer().descriptor

    override fun serialize(encoder: Encoder, value: Operator) =
        throw RuntimeException("Serialization to FRIDA format is not supoprted")

    override fun deserialize(decoder: Decoder): Operator {
        val fridaContractor = decoder.decodeSerializableValue(FridaContractor.serializer())
        return Operator(
            sourceId = fridaContractor.Id,
            ref = "RUT:Operator:${fridaContractor.CustomersGID}",
            name = fridaContractor.Name,
        )
    }

    @Serializable
    @SerialName("Contractor")
    data class FridaContractor(
        val Id: Long,
        val Name: String,
        val CustomersGID: Long?,
    )
}
