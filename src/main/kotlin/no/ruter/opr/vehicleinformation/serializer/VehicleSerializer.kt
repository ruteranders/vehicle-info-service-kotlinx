package no.ruter.opr.vehicleinformation.serializer

import no.ruter.opr.vehicleinformation.domain.Operator
import no.ruter.opr.vehicleinformation.domain.Vehicle
import no.ruter.opr.vehicleinformation.service.ApiService
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDateTime.parse

object VehicleSerializer : KSerializer<Vehicle> {
    lateinit var apiService: ApiService
    private var operatorsBySourceId: Map<Long, Operator> = emptyMap()
    private var vehicleTypesBySourceId: Map<Long, VehicleType> = emptyMap()
    private var engineTypesBySourceId: Map<Long, EngineType> = emptyMap()
    override val descriptor: SerialDescriptor = FridaVehicle.serializer().descriptor

    fun refreshMetadata() {
        operatorsBySourceId = apiService.getOperators().associateBy { it.sourceId }
        vehicleTypesBySourceId = apiService.getVehicleTypes().associateBy { it.sourceId }
        engineTypesBySourceId = apiService.getEngineTypes().associateBy { it.sourceId }
    }

    override fun serialize(encoder: Encoder, value: Vehicle) =
        throw RuntimeException("Serialization to FRIDA format is not supported")

    override fun deserialize(decoder: Decoder): Vehicle {
        val fridaVehicle = decoder.decodeSerializableValue(FridaVehicle.serializer())
        return Vehicle(
            sourceId = fridaVehicle.Id,
            internalNumber = fridaVehicle.InternalNumber,
            registrationNumber = fridaVehicle.PublicIDString,
            chassisNumber = fridaVehicle.ChassiNumber,
            operatorRef = operatorsBySourceId[fridaVehicle.Contractor.Id]?.ref ?: "UNKNOWN",
            trafficStart = parse(fridaVehicle.TrafficStart),
            trafficEnd = parse(fridaVehicle.TrafficEnd),
            totalCapacity = fridaVehicle.TotalNumberOfPassengers,
            seatedCapacity = fridaVehicle.NumberOfPassengersSeated,
            vehicleType = vehicleTypesBySourceId[fridaVehicle.VehicleTypeId]?.name
                ?: "UNKNOWN",
            vehicleSize = fridaVehicle.VehicleSize,
            engineType = engineTypesBySourceId[fridaVehicle.EngineTypeId]?.name ?: "UNKNOWN",
        )
    }

    @Serializable
    @SerialName("Vehicle")
    data class FridaVehicle(
        val Id: Long,
        val InternalNumber: Long?,
        val PublicIDString: String?,
        val ChassiNumber: String?,
        val Contractor: FridaContractorRef,
        val TrafficStart: String,
        val TrafficEnd: String,
        val TotalNumberOfPassengers: Short?,
        val NumberOfPassengersSeated: Short?,
        val VehicleTypeId: Long,
        val VehicleSize: String?,
        val EngineTypeId: Long,
    ) {
        @Serializable
        data class FridaContractorRef(val Id: Long)
    }

    @Serializable
    data class VehicleType(
        @SerialName("Id") val sourceId: Long,
        @SerialName("Name") val name: String,
        @SerialName("UseChassiNumber") val hasChassisNumber: Boolean
    )

    @Serializable
    data class EngineType(
        @SerialName("Id") val sourceId: Long,
        @SerialName("Name") val name: String,
    )
}