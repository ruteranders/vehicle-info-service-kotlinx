package no.ruter.opr.vehicleinformation

import mu.KotlinLogging
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class VehicleInfoServiceSimplifiedApplication

private val logger = KotlinLogging.logger {}

fun main(args: Array<String>) {
    runApplication<VehicleInfoServiceSimplifiedApplication>(*args)

    Thread.setDefaultUncaughtExceptionHandler { thread, e ->
        logger.error(e) { "[$thread] Uncaught exception: ${e.message}" }
    }
}
