package no.ruter.opr.vehicleinformation.domain

import no.ruter.opr.vehicleinformation.serializer.VehicleSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDateTime

@Serializable(with = VehicleSerializer::class)
data class Vehicle(
    val sourceId: Long,
    val internalNumber: Long?,
    val registrationNumber: String?,
    val chassisNumber: String?,
    val operatorRef: String,
    val trafficStart: LocalDateTime,
    val trafficEnd: LocalDateTime,
    val totalCapacity: Short?,
    val seatedCapacity: Short?,
    val vehicleType: String,
    val vehicleSize: String?,
    val engineType: String,
    override val id: Long? = null,
    override val createdBy: String? = null,
    override val createdAt: LocalDateTime? = null,
    override val updatedBy: String? = null,
    override val updatedAt: LocalDateTime? = null,
) : Entity
