package no.ruter.opr.vehicleinformation.domain

import java.time.LocalDateTime

interface Entity {
    val id: Long?
    val createdBy: String?
    val createdAt: LocalDateTime?
    val updatedBy: String?
    val updatedAt: LocalDateTime?
}
