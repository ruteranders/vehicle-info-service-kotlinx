package no.ruter.opr.vehicleinformation.domain

import no.ruter.opr.vehicleinformation.serializer.OperatorSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDateTime

@Serializable(with = OperatorSerializer::class)
data class Operator(
    val sourceId: Long,
    val ref: String,
    val name: String,
    override val id: Long? = null,
    override val createdBy: String? = null,
    override val createdAt: LocalDateTime? = null,
    override val updatedBy: String? = null,
    override val updatedAt: LocalDateTime? = null,
) : Entity