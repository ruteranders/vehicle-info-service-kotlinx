package no.ruter.opr.vehicleinformation.service

import no.ruter.opr.vehicleinformation.serializer.VehicleSerializer
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class VehicleInformationService(
    private val apiService: ApiService,
) {
    init {
        VehicleSerializer.apiService = apiService
    }

    @Scheduled(cron = "0 0 23 * * *", zone = "Europe/Oslo")
    fun refreshVehicleInformation() {
        VehicleSerializer.refreshMetadata()
        apiService.getOperators().forEach {
            TODO("Persist to DB")
        }
        apiService.getVehicles().forEach {
            TODO("Persist to DB")
        }
    }
}
