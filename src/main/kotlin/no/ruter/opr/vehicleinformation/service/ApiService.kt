package no.ruter.opr.vehicleinformation.service

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import no.ruter.opr.vehicleinformation.config.ApiCredentials
import no.ruter.opr.vehicleinformation.domain.Operator
import no.ruter.opr.vehicleinformation.domain.Vehicle
import no.ruter.opr.vehicleinformation.serializer.VehicleSerializer
import okhttp3.*
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.Instant.now

@Service
class ApiService(
    private val apiCredentials: ApiCredentials,
) {
    private val httpClient = OkHttpClient()
    private val json = Json { ignoreUnknownKeys = true }
    private lateinit var accessToken: String
    private lateinit var accessTokenExpiresAt: Instant

    fun getOperators(): List<Operator> =
        executeGetRequest("system/contractors")

    fun getVehicles(): List<Vehicle> =
        executeGetRequest("vehicle/vehicles")

    fun getVehicleTypes(): List<VehicleSerializer.VehicleType> =
        executeGetRequest("vehicle/vehicletypes")

    fun getEngineTypes(): List<VehicleSerializer.EngineType> =
        executeGetRequest("vehicle/enginetypes")

    private inline fun <reified T> executeGetRequest(resourcePath: String): List<T> =
        httpClient
            .newCall(
                Request.Builder()
                    .header("Authorization", "Bearer ${validAccessToken()}")
                    .url("${apiCredentials.apiUri}/${resourcePath}?page=1&pagesize=2000&api-version=1.3")
                    .build()
            )
            .execute()
            .use { response ->
                json.decodeFromString<FridaResponse<T>>(jsonString(response)).items
            }

    private fun validAccessToken(): String =
        if (::accessToken.isInitialized && now().isBefore(accessTokenExpiresAt.minusSeconds(120)))
            accessToken
        else
            with(apiCredentials) {
                httpClient
                    .newCall(
                        Request.Builder()
                            .header("Authorization", Credentials.basic(clientId, clientSecret))
                            .url("${authUri}/connect/token")
                            .post(
                                FormBody.Builder()
                                    .add("grant_type", "password")
                                    .add("username", username)
                                    .add("password", password)
                                    .add("scope", scope)
                                    .build()
                            )
                            .build()
                    )
                    .execute()
                    .use { response ->
                        with(json.decodeFromString<TokenResponse>(jsonString(response))) {
                            this@ApiService.accessToken = accessToken
                            accessTokenExpiresAt = now().plusSeconds(expiresIn)
                            accessToken
                        }
                    }
            }

    private fun jsonString(response: Response): String {
        if (!response.isSuccessful) {
            throw RuntimeException("Request failed (${response.request.url}): ${response.code}")
        }
        return response.body?.string()
            ?: throw RuntimeException("Request (${response.request.url}) returned with empty body")
    }

    @Serializable
    data class FridaResponse<T>(
        @SerialName("Items") val items: List<T>
    )

    @Serializable
    data class TokenResponse(
        @SerialName("access_token") val accessToken: String,
        @SerialName("expires_in") val expiresIn: Long,
        @SerialName("token_type") val tokenType: String,
        val scope: String,
    )
}