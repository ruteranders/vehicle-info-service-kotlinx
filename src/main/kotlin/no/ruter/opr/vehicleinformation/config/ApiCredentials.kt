package no.ruter.opr.vehicleinformation.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "credentials.frida")
data class ApiCredentials(
    val authUri: String,
    val apiUri: String,
    val clientId: String,
    val clientSecret: String,
    val username: String,
    val password: String,
    val scope: String,
)
